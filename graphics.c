#include "graphics.h"

int createImage(Image *image, int width, int height)
{
    image->red = calloc(width * height,sizeof(unsigned char));
    if(image->red == NULL) { return 0; }
    image->green = calloc(width * height,sizeof(unsigned char));
    if(image->green == NULL) { return 0; }
    image->blue = calloc(width * height,sizeof(unsigned char));
    if(image->blue == NULL) { return 0; }
    image->width = width;
    image->height = height;
    return 1;
}

void deleteImage(Image *image)
{
    free(image->red);
    free(image->green);
    free(image->blue);
}

void drawPixel(Image *image, Vec2 pos, Color color)
{
    if(pos.x < image->width && pos.x > 0 && pos.y < image->height && pos.y > 0)
    {
        image->red[(pos.y)*image->width + pos.x] = color.red;
        image->green[(pos.y)*image->width + pos.x] = color.green;
        image->blue[(pos.y)*image->width + pos.x] = color.blue;
    }
}

void drawLine(Image *image, Vec2 pos1, Vec2 pos2, Color color)
{
    int dx = abs(pos2.x-pos1.x), xDir = pos1.x<pos2.x ? 1 : -1; 
    int dy = abs(pos2.y-pos1.y), yDir = pos1.y<pos2.y ? 1 : -1; 
    int err = (dx>dy? dx : -dy)/2, e2;

    while(1)
    {
        drawPixel(image, pos1, color);
        if(pos1.x == pos2.x && pos1.y == pos2.y) break;
        e2 = err;
        if (e2 >-dx) { err -= dy; pos1.x += xDir; }
        if (e2 < dy) { err += dx; pos1.y += yDir; }
    }
}

void drawQuad(Image *image, Vec2 a, Vec2 b, Vec2 c, Vec2 d, Color color)
{
    drawLine(image, a, b, color);
    drawLine(image, b, c, color);
    drawLine(image, c, d, color);
    drawLine(image, d, a, color);
}

void drawTree(Image *image, int it, Vec2 a, Vec2 b, ColorPalette *cp)
{
    if(!it) return;
    Vec2 df = { b.x - a.x, a.y - b.y };
    Vec2 c = {b.x - df.y, b.y - df.x};
    Vec2 d = {a.x - df.y, a.y - df.x};
    Vec2 e = {d.x + (( df.x - df.y) / 2), d.y - ((df.x + df.y) / 2)};
    drawQuad(image, a,b,c,d, cp->color[it%cp->count]);
    drawTree(image, it-1, d, e, cp);
    drawTree(image, it-1, e, c, cp);
}

int generateColorPalette(ColorPalette *cp, int count)
{
    cp->count = count;
    cp->color = calloc(count, sizeof(Color));
    if(cp->color == NULL) { return 0; }
    srand(time(NULL));
    int i;
    for(i=0; i<count; i++)
    {
        set_color(cp->color[i], rand()%256, rand()%256, rand()%256);
    }
    return 1;
}

void deleteColorPalette(ColorPalette *cp)
{
    free(cp->color);
}
