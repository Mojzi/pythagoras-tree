#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graphics.h"

#define printUsage() fputs("Usage: pythagoras-tree rootsize recursion-depth colornumber filename\n", stdout);

int writeImageToFile(Image *image, const char *fileName)
{
    FILE *fp = fopen(fileName, "wb");
    if(fp == NULL) { return 0; }
    fprintf(fp, "P6 %d %d %d\n", image->width, image->height, COLOR_MAX_VAL);
    int x, imageSize = image->width*image->height;
    for(x = 0; x < imageSize; x++)
    {
            fwrite(&image->red[x], sizeof(unsigned char), 1, fp);
            fwrite(&image->green[x], sizeof(unsigned char), 1, fp);
            fwrite(&image->blue[x], sizeof(unsigned char), 1, fp);
    }
    fclose(fp);
    return 1;
}


int setParameters(const char **argv, int *fractalSize, int *treeDepth, int *colorsNumber, char *filename, int *width, int *height)
{
    *fractalSize = atoi(argv[1]);
    if( *fractalSize <= 0 ) { return 0; }
    *treeDepth = atoi(argv[2]);
    if( *treeDepth <= 0 ) { return 0; }
    *colorsNumber = atoi(argv[3]);
    if( *colorsNumber <= 0 ) { return 0; }
    strcpy(filename, argv[4]);
    *width = *fractalSize*6;
    *height = *fractalSize*4;
    return 1;
}

void setOrigin(Vec2 *left, Vec2 *right, int imageWidth, int imageHeight, int fractalSize)
{
    left->x = imageWidth/2 - fractalSize/2;
    left->y = imageHeight-1;
    right->x = imageWidth/2 + fractalSize/2;
    right->y = imageHeight-1;
}

int main(int argc, const char **argv)
{
    char filename[BUFSIZ];
    int fractalSize;
    int colorsNumber;
    int treeDepth;
    int imageWidth;
    int imageHeight;
    Vec2 leftOrigin;
    Vec2 rightOrigin;
    Image image;
    ColorPalette cp;
    if(argc != 5) { printUsage(); return 1;}
    if(setParameters(argv, &fractalSize, &treeDepth, &colorsNumber, filename, &imageWidth, &imageHeight) == 0) { printUsage(); return 1; }
    if(generateColorPalette(&cp, colorsNumber) == 0)
    {
        fputs("Failed to generate color Palette! Aborting...\n", stderr);
        return 1;
    }
    if(createImage(&image, imageWidth, imageHeight) == 0)
    {
        fputs("Failed to create Image! Aborting...\n", stderr);
        return 1;
    }
    setOrigin(&leftOrigin, &rightOrigin, imageWidth, imageHeight, fractalSize);
    drawTree(&image, treeDepth, leftOrigin, rightOrigin, &cp);
    if(writeImageToFile(&image, filename) == 0)
    {
        fputs("Couldn't write image to file!\n", stderr);
        return 1;
    }
    deleteImage(&image);
    fputs("Done!\n", stderr);
    return 0;
}
