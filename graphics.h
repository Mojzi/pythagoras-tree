#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <math.h>
#include <time.h>
#include <stdlib.h>

#define COLOR_MAX_VAL 255
#define set_color(color, rc, gc, bc) (color).red = (rc); (color).green = (gc); (color).blue = (bc)

typedef struct
{
    int width;
    int height;
    unsigned char *red;
    unsigned char *green;
    unsigned char *blue;
} Image;

typedef struct
{
    int x, y;
} Vec2;

typedef struct
{
    unsigned char red;
    unsigned char green;
    unsigned char blue;
} Color;

typedef struct
{
    int count;
    Color *color;
} ColorPalette;

void drawTree(Image *image, int it, Vec2 a, Vec2 b, ColorPalette *cp);
int createImage(Image *image, int width, int height);
void deleteImage(Image *image);
int generateColorPalette(ColorPalette *cp, int count);
void deleteColorPalette(ColorPalette *cp);

#endif
